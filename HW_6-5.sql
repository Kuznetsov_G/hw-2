select round((sysdate - hire_date)/365,0) as Experience,
 count (employee_id)
 from employees
group by round((sysdate - hire_date)/365,0)
order by Experience;