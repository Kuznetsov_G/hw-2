select last_name as "name", (commission_pct * salary + salary) as "salary" from employees
where commission_pct is not null 
order by last_name;