select job_id, last_name, salary,
decode (job_id,'IT_PROG', 1.10*salary,
               'SH_CLERK',0.9*salary) as new_salary            
from employees
where job_id in ('IT_PROG','SH_CLERK')
order by job_id asc, new_salary desc;