select to_char(hire_date,'Month') as "release_date", count(job_id) as "count" from employees
group by to_char(hire_date,'Month')
order by count(job_id) asc;