drop table agreement_payments ;
drop view ap_view;
drop synonym ap;
--drop index Index1;
drop sequence s_payments;
create table agreement_payments (
agreement_id integer,
agreement_number integer,
pay_value number,
pay_date date);
create index Index1 
on agreement_payments (pay_date,pay_value);
create sequence s_payments
increment by 1
start with 101
maxvalue 10000
nocache
nocycle;
insert into agreement_payments values(s_payments.nextval,1001,373,'04-01-2018');
insert into agreement_payments values(s_payments.nextval,302,1022,'03-02-2015');
insert into agreement_payments values(s_payments.nextval,665,123,'24-11-2018');
insert into agreement_payments values(s_payments.nextval,999,2223,'24-11-2018');
insert into agreement_payments values(s_payments.nextval,5632,1,'19-05-2015');
insert into agreement_payments values(s_payments.nextval,137,3454,'22-11-2002');
select s_payments.currval
from dual;
create synonym ap
for agreement_payments;
create view ap_view 
as select pay_date,sum(pay_value) as sum_pays, count(pay_date) as num_pays
from ap
group by pay_date;
select * from ap_view;
/