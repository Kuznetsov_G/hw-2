select distinct department_id, 
 round(avg(salary * nvl(commission_pct,0) + salary),0) as "new_salary",
 round(avg(salary),0) as "old_salary",
 round(avg(salary * nvl(commission_pct,0))) as "diff"
 from employees
group by department_id order by round(avg(salary * nvl(commission_pct,0))) asc;