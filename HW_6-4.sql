select manager_id, 
count(employee_id) as Quantity, 
round(avg((sysdate - hire_date)/7),0) as Experience
 from employees
group by manager_id;